# network

Notes on safe and convenient network setup.

## Packages

- `unbound` - serves as a caching DNS resolver.
- `expat` - provides DNSSEC validation.
- `dnscrypt-` - serves as a DNS proxy, provides encryption, DNSSEC compatible.

## Setup

- `# vi /etc/udev/rules.d/10-network.rules` - create the rules for renaming the network interfaces. Find the original addresses using `ip a`.

````
SUBSYSTEM=="net", ACTION=="add", ATTR{address}=="01:23:45:67:89:ab", NAME="wired"
SUBSYSTEM=="net", ACTION=="add", ATTR{address}=="11:22:33:44:55:66", NAME="wireless"
````

- `# vi /etc/systemd/network/00-default.link` - spoof the original MAC addresses.

````
[Match]
MACAddress=01:23:45:67:89:ab 11:22:33:44:55:66

[Link]
MACAddressPolicy=random
NamePolicy=kernel database onboard slot path
````

- `# vi /etc/systemd/network/25-wired-wireless.network` - configure the DHCP for both interfaces.

````
[Match]
Name=wired wireless

[Network]
DHCP=ipv4
````

- `# systemctl enable systemd-networkd.service` - enable the systemd network service.

- `# vi /etc/resolv.conf` - edit the resolver config, forward to unbound.

````
#use local DNS cache (unbound)
nameserver ::1
nameserver 127.0.0.1
options edns0 single-request-reopen
````

- `# vi /etc/unbound/unbound.conf` - edit the unbound config and forward to dnscrypt.

````
server:
	use-syslog: yes
	do-daemonize: no
	username: "unbound"
	directory: "/etc/unbound"
	trust-anchor-file: trusted-key.key
	do-not-query-localhost: no

forward-zone:
	name: "."
	#use dnscrypt
	forward-addr: ::1@53000
	forward-addr: 127.0.0.1@53000
````

- `# vi /etc/dnscrypt-proxy/dnscrypt-proxy.toml` - edit the dnscrypt config. Using specific server names will speed things up but the server has to support DNSSEC. If unsure, leave the `server_names` empty and enable `require_dnssec`.

````
...
server_names = []
...
require_dnssec = true
````
